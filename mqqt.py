#!/usr/bin/python3

import glob
import time
import paho.mqtt.client as mqtt

Broker = '192.168.1.170'
auth = {
    'username': 'pi',
    'password': 'raspberry',
}

pub_topic = 'octoprint/temperature/'

base_dir = '/sys/bus/w1/devices/'


def read_temp():
    valid = False
    temp = 0
    result = {}
    for device_folder in glob.glob(base_dir + '28-*'):
        device_file = device_folder + '/w1_slave'
        with open(device_file, 'r') as f:
            for line in f:
                if line.strip()[-3:] == 'YES':
                    valid = True
                temp_pos = line.find(' t=')
                if temp_pos != -1:
                    temp = float(line[temp_pos + 3:]) / 1000.0
        if valid:
            result[device_folder.replace(base_dir, "").replace('/w1_slave', "")] = temp

    for device_folder in glob.glob(base_dir + '26-*'):
        temp = 0
        vdd = 0
        vad = 0
        with open(device_folder + '/temperature', 'r') as f:
            for line in f:
                temp = float(line) / 255.0
        with open(device_folder + '/vdd', 'r') as f:
            for line in f:
                vdd = float(line) / 100.0
        with open(device_folder + '/vad', 'r') as f:
            for line in f:
                vad = float(line) / 100.0
        print("tem {0} vdd {1} vad {2}".format(temp, vdd, vad))
        humid = (vad / vdd - 0.1515) / 0.00636 / (1.0546 - 0.00216 * temp)
        result[device_folder.replace(base_dir, "")] = humid
    return result


while True:
    client = mqtt.Client("octoprint")
    client.username_pw_set("pi", "raspberry")
    temp = read_temp()
    for topic in temp:
        print(pub_topic + topic)
        print(str(temp[topic]))
        client.connect(Broker)
        client.publish(pub_topic + topic, str(temp[topic]))
        client.disconnect()
    time.sleep(30)
